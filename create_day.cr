# TODO Creates a template for the given day

require "option_parser"

day_to_create = Time.local.day

OptionParser.parse do |parser|
  parser.on "-d DAY", "--day=DAY", "Select which day you want to run" do |day|
    day_to_create = day
  end
end

template_content = File.read("./days/day_template")
day_content = template_content.gsub("{day_index}", day_to_create)

new_file_name = "./days/day#{day_to_create}.cr"
puts "Creating #{new_file_name}"
File.write(new_file_name, day_content)


puts "Updating cli.cr for new day"
existing_content = File.read_lines("./cli.cr")
new_day_index = existing_content.index("## Insert new day line")
if new_day_index.nil?
  puts "Cannot find line '## Insert new day line'"
  exit
end

existing_content.insert(new_day_index, "when \"#{day_to_create}\"")
existing_content.insert(new_day_index + 1, "  day_#{day_to_create}(part_to_run)")
new_content = existing_content.reduce("") { |acc, line|acc + "\n" + line } 
File.write("./cli.cr", new_content)