# Advent of Code 2021

For this year, I'll be learning the Crystal Language. As a professed non-rubyist this language
will likely not come naturally. The hope however is that this exercise will breed some familiarity


## Running Days
Running both parts of a given day:
```shell
crystal run cli.cr -- -d 1
```

Running a single part of a given day:
```shell
crystal run cli.cr -- -d 1 -p 1
```


Create a new day with:
```shell
crystal run create_day.cr
```
This command automatically detects the day of the month from the system time. To override this, use -d.
```shell
crystal run create_day.cr -d 2
```