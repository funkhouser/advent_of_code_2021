def day_13(part)
  puts "Day 13: https://adventofcode.com/2021/day/13"
  puts "Day 13 Input: https://adventofcode.com/2021/day/13/input"
  case part
  when "1"
    day_13_part_1()
  when "2"
    day_13_part_2()
  else
    day_13_part_1()
    day_13_part_2()
  end
end


def day_13_part_1()
  puts "Day 13: Part 1"
  input_file = "./inputs/day13"
  folds = [] of Tuple(String, Int32)
  dots = [] of Tuple(Int32, Int32)
  File.read_lines(input_file).each do |line|
    if line.starts_with? "fold"
      result = line.split(" ")[2].strip.split("=")
      folds.push({result[0], Int32.new(result[1])})
    elsif line.includes? ","
      dot = line.split(",")
      dots.push({Int32.new(dot[0]), Int32.new(dot[1])})
    end
  end

  puts dots
  puts folds

  (0...1).each do |fold_index|
    fold = folds[fold_index]
    is_x_fold = fold[0] == "x"
    dots = (dots.map do |dot|
      if is_x_fold
        if dot[0] < fold[1]
          dot
        else
          x = fold[1] - (dot[0] - fold[1])
          {x, dot[1]}
        end
      else
        if dot[1] < fold[1]
          dot
        else
          y = fold[1] - (dot[1] - fold[1])
          {dot[0], y}
        end
      end
    end).uniq
  end
  puts dots
  puts "Count: #{dots.size}"
end

def day_13_part_2()
  puts "Day 13: Part 2"

  input_file = "./inputs/day13"
  folds = [] of Tuple(String, Int32)
  dots = [] of Tuple(Int32, Int32)
  File.read_lines(input_file).each do |line|
    if line.starts_with? "fold"
      result = line.split(" ")[2].strip.split("=")
      folds.push({result[0], Int32.new(result[1])})
    elsif line.includes? ","
      dot = line.split(",")
      dots.push({Int32.new(dot[0]), Int32.new(dot[1])})
    end
  end
  (0...folds.size).each do |fold_index|
    fold = folds[fold_index]
    is_x_fold = fold[0] == "x"
    dots = (dots.map do |dot|
      if is_x_fold
        if dot[0] < fold[1]
          dot
        else
          x = fold[1] - (dot[0] - fold[1])
          {x, dot[1]}
        end
      else
        if dot[1] < fold[1]
          dot
        else
          y = fold[1] - (dot[1] - fold[1])
          {dot[0], y}
        end
      end
    end).uniq
  end

  width = dots.sort_by { |d| d[0] }[-1][0]
  height = dots.sort_by { |d| d[1] }[-1][1]

  (0..height).each do |y|
    (0..width).each do |x|
      if dots.includes?({x, y})
        print "#"
      else
        print "."
      end
    end
    print "\n"
  end
end