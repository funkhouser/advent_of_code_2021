def day_15(part)
  puts "Day 15: https://adventofcode.com/2021/day/15"
  puts "Day 15 Input: https://adventofcode.com/2021/day/15/input"
  case part
  when "1"
    day_15_part_1()
  when "2"
    day_15_part_2()
  else
    day_15_part_1()
    day_15_part_2()
  end
end


def day_15_part_1()
  puts "Day 15: Part 1"
  puts "Discover total risk for the traversal"

  input_file = "./inputs/day15"
  total_grid = File.read_lines(input_file).map do |line|
    line.chars.map { |c| Int32.new(c) }
  end

  # costs_graph = [] of Tuple()  
  # total_grid.each_with_index do |row, row_index|
  #   row.each_with_index do |cell, col_index|
  #     puts "abc"
  #   end
  # end

  costs_grid : Array(Array(Int32)) = total_grid.map do |row|
    row.map { |c| 10000000 }
  end

  costs_grid[-1][-1] = total_grid[-1][-1]

  deltas = [{-1, 0}, {1, 0}, {0, -1}, {0, 1}]
  row_range = (0...(total_grid.size))
  col_range = (0...(total_grid[0].size))
  distance_range = (0...(total_grid.size + total_grid[0].size))
  value_find = ->(row : Int32, col : Int32) { 
    entry_cost = total_grid[row][col]

    min_cost = costs_grid[row][col]
    deltas.each do |d|
      row0 = row + d[0]
      col0 = col + d[1]

      if row_range.includes?(row0) && col_range.includes?(col0)
        if min_cost > (costs_grid[row0][col0] + entry_cost)
          min_cost = (costs_grid[row0][col0] + entry_cost)
        end
      end
    end
    costs_grid[row][col] = min_cost
    min_cost
  }

  found_optimization = true
  i = 0
  while found_optimization
    i += 1
    found_optimization = false
    distance_range.each do |distance|
      (0..distance).each do |row|
        col = distance - row
        curr_val = costs_grid[row_range.end - 1 - row][col_range.end - 1 - col]
        new_val = value_find.call(row_range.end - 1 - row, col_range.end - 1 - col)
        found_optimization = (new_val < curr_val) || found_optimization
      end 
    end
    # puts costs_grid
  end

  puts costs_grid
  puts costs_grid[0][0] - total_grid[0][0]
end

def day_15_part_2()
  puts "Day 15: Part 2"
  input_file = "./inputs/day15"
  total_grid = File.read_lines(input_file).map do |line|
    line.chars.map { |c| Int32.new(c) }
  end

  total_grid = (0...5).flat_map do |copy_row_index|
    total_grid.map_with_index do |row, row_index|
      (0...5).flat_map do |copy_col_index|
        row.map_with_index do |cell, col_index|
          if cell + copy_col_index + copy_row_index >= 10
            cell + copy_col_index + copy_row_index - 9
          else
            cell + copy_col_index + copy_row_index
          end
        end
      end
    end
  end

  puts total_grid

  costs_grid : Array(Array(Int32)) = total_grid.map do |row|
    row.map { |c| 10000000 }
  end

  costs_grid[-1][-1] = total_grid[-1][-1]

  deltas = [{-1, 0}, {1, 0}, {0, -1}, {0, 1}]
  row_range = (0...(total_grid.size))
  col_range = (0...(total_grid[0].size))
  distance_range = (0...(total_grid.size + total_grid[0].size))
  value_find = ->(row : Int32, col : Int32) { 
    entry_cost = total_grid[row][col]

    min_cost = costs_grid[row][col]
    deltas.each do |d|
      row0 = row + d[0]
      col0 = col + d[1]

      if row_range.includes?(row0) && col_range.includes?(col0)
        if min_cost > (costs_grid[row0][col0] + entry_cost)
          min_cost = (costs_grid[row0][col0] + entry_cost)
        end
      end
    end
    costs_grid[row][col] = min_cost
    min_cost
  }

  found_optimization = true
  i = 0
  while found_optimization
    i += 1
    found_optimization = false
    distance_range.each do |distance|
      (0..distance).each do |row|
        col = distance - row
        curr_val = costs_grid[row_range.end - 1 - row][col_range.end - 1 - col]
        new_val = value_find.call(row_range.end - 1 - row, col_range.end - 1 - col)
        found_optimization = (new_val < curr_val) || found_optimization
      end 
    end
    # puts costs_grid
  end

  # puts costs_grid
  puts "Iterations #{i}"
  puts "Result #{costs_grid[0][0] - total_grid[0][0]}"
end