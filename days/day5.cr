def day_5(part)
  puts "Day 5: https://adventofcode.com/2021/day/5"
  puts "Day 5 Input: https://adventofcode.com/2021/day/5/input"
  case part
  when "1"
    day_5_part_1()
  when "2"
    day_5_part_2()
  else
    day_5_part_1()
    day_5_part_2()
  end
end


def day_5_part_1()
  puts "Day 5: Part 1"
  puts "Hydrothermic vents oh my. Find how many intersecting points there are counting only vertical / horizontal lines"

  lines = [] of Line
  File.read_lines("./inputs/day5").each do |line|
    p0 = line.split("->")[0].strip()
    p1 = line.split("->")[1].strip()
    
    lines << Line.new(
      Int32.new(p0.split(",")[0]),
      Int32.new(p0.split(",")[1]),
      Int32.new(p1.split(",")[0]),
      Int32.new(p1.split(",")[1]))
  end
  
  points = (lines.map do |line|
    if line.x0 == line.x1
      start = Math.min(line.y0, line.y1)
      line_end = Math.max(line.y0, line.y1)
      ((start..line_end).map do |y|
        Point.new(line.x0, y)
      end).to_a
    elsif line.y0 == line.y1
      start = Math.min(line.x0, line.x1)
      line_end = Math.max(line.x0, line.x1)
      ((start..line_end).map do |x|
        Point.new(x, line.y0)
      end)
    else
      [] of Point
    end
  end).flatten

  # points.each {|p| puts p }
  puts points
  final_points = points.sort.each_cons(2).select { |a| a[0] == a[1] }.flatten.uniq.to_a
  puts final_points

  puts "Overlapping points: #{final_points.size}"
end


def day_5_part_2()
  puts "Day 5: Part 2"
  puts "Hydrothermic vents oh my. Find how many intersecting points there are counting only vertical / horizontal / diagonal lines"

  lines = [] of Line
  File.read_lines("./inputs/day5").each do |line|
    p0 = line.split("->")[0].strip()
    p1 = line.split("->")[1].strip()
    
    lines << Line.new(
      Int32.new(p0.split(",")[0]),
      Int32.new(p0.split(",")[1]),
      Int32.new(p1.split(",")[0]),
      Int32.new(p1.split(",")[1]))
  end
  
  points = (lines.map do |line|
    if line.x0 == line.x1
      start = Math.min(line.y0, line.y1)
      line_end = Math.max(line.y0, line.y1)
      (start..line_end).map do |y|
        Point.new(line.x0, y)
      end
    elsif line.y0 == line.y1
      start = Math.min(line.x0, line.x1)
      line_end = Math.max(line.x0, line.x1)
      (start..line_end).map do |x|
        Point.new(x, line.y0)
      end
    elsif line.x0 + line.y0 == line.x1 + line.y1
      sign = line.x0 > line.x1 ? -1 : 1
      (0..(line.x1 - line.x0).abs).map do |i|
        Point.new(line.x0 + i * sign, line.y0 - i * sign)
      end
    elsif (line.x1 - line.x0) == (line.y1 - line.y0) # line.x0 + line.x1 == line.y0 + line.y1
      # See https://www.therelicans.com/wyhaines/implementing-a-ruby-like-send-in-crystal-2afc
      # In ruby, using the send on the op would work
      # For crystal, basically the solution is to create a lookup of each method typing via a macro
      # and then pass in accordingly to be type-safe. This isn't implemented by default
      # op = line.x0 > line.x1 ? :- : :+
      # (0..(line.x1 - line.x0).abs).map do |i|
      #   Point.new(line.x0.send(op, i), line.y0.send(op, -i))
      # end
      sign = line.x0 > line.x1 ? -1 : 1
      (0..(line.x1 - line.x0).abs).map do |i|
        Point.new(line.x0 + i * sign, line.y0 + i * sign)
      end
    else
      [] of Point
    end
  end).flatten

  # points.each {|p| puts p }
  puts points
  final_points = points.sort.each_cons(2).select { |a| a[0] == a[1] }.flatten.uniq.to_a
  puts final_points

  puts "Overlapping points: #{final_points.size}"
end



struct Point
  property x, y
  def initialize(@x : Int32, @y : Int32)
  end

  def ==(other : self) : Bool
    @x == other.x && @y == other.y
  end

  def <=>(other : self) : Int32
    x_comp = @x <=> other.x 
    if x_comp == 0
      @y <=> other.y
    else
      x_comp
    end
  end

  # to_s overrides the put for printing the element
  def to_s(io : IO) : Nil
    io << "(#{@x},#{@y})"
  end

  # Arrays however use inspect on the individual elements instead (when called to_s via puts)
  def inspect(io : IO)
    io << "(#{@x},#{@y})"
  end
end

struct Line
  property x0, y0, x1, y1
  def initialize(@x0 : Int32, @y0 : Int32, @x1 : Int32, @y1 : Int32)
  end
end