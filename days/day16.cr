def day_16(part)
  puts "Day 16: https://adventofcode.com/2021/day/16"
  puts "Day 16 Input: https://adventofcode.com/2021/day/16/input"
  case part
  when "1"
    day_16_part_1()
  when "2"
    day_16_part_2()
  else
    day_16_part_1()
    day_16_part_2()
  end
end


def day_16_part_1()
  puts "Day 16: Part 1"
  input_file = "./inputs/day16"
  total_bits = File.read(input_file).strip().chars.flat_map {|char| Hex.hex_value(char) }.join()
  puts total_bits

  i = Int64.new(0)
  top_nodes = [] of Node
  while i < total_bits.size
    node, next_index = read_packet(total_bits, i)
    if !node.nil?
      top_nodes.push(node.as(Node))
    end
    i = next_index
    puts node
    puts i
    puts ""
  end

  version_sum = Int64.new(0)
  
  #  This is the more pythonic way to traverse rather than ruby.
  # When trying to do the walk via yields, the compiler would recursively error out
  # with no reasonable information.
  top_nodes.each do |n|
    # version_sum += n.version
    traverse(n) { |a| version_sum += a.version }
  end
    # version_sum += n.version
  # n.walk do
    # puts "#{n0} #{n0.version}"
    # puts "#{n0}"
    # version_sum += n0.version
  # end
  # n.walk do |node|
  #   puts node
  # end
  puts "Version sum #{version_sum}"
end

def traverse(node, &block : Node -> _)
  block.call(node)
  node.children.each { |n| traverse(n, &block) }
end

def traverse_two(node : Node, &block : (Node, Array(Int64)) -> Int64)
  child_vals : Array(Int64) = node.children.map { |n| traverse_two(n, &block).as(Int64) }
  block.call(node, child_vals)
end

def traverse_rev(node : Node, string : String, &block : (Node, String) -> String)
  new_string = block.call(node, string)
  node.children.map { |n| traverse_rev(n, new_string, &block).as(String) }
  string
end


def day_16_part_2()
  puts "Day 16: Part 2"
  input_file = "./inputs/day16"
  total_bits = (File.read(input_file).strip().chars.flat_map do |char|
    puts char
    puts Hex.hex_value(char)
    Hex.hex_value(char)
  end).join()
  puts total_bits

  i = Int64.new(0)
  top_nodes = [] of Node
  node, next_index = read_packet(total_bits, i)
  if !node.nil?
    top_nodes.push(node.as(Node))
  end
  i = next_index
  puts ""
  if i + 15 < total_bits.size
    puts "This shouldn't happen"
    exit
  end

  top_nodes.each do |n|
    traverse_rev(n, "") do |node, prefix|
      if node.type_id == 4
        puts "#{prefix}#{node.type_id} #{node.as(LiteralNode).value}"
      else
        puts "#{prefix}#{node.type_id}. Children: #{node.children.size}"
      end  
      "#{prefix} "
    end
  end


  top_nodes.each do |n|
    # version_sum += n.version
    my_val = traverse_two(n) do |node, child_vals|
      a = case node.type_id
      when 0
        puts "#{child_vals} 0"
        child_vals.reduce(Int64.new(0)) { |acc, v| acc + v }
      when 1
        puts "#{child_vals} 1"
        b = child_vals.reduce(Int64.new(1)) { |acc, v| acc * v}
        b = b < 0 ? Int64.new(43234) : b
      when 2
        puts "#{child_vals} 2"
        child_vals.min
      when 3
        puts "#{child_vals} 3"
        child_vals.max
      when 4
        puts "Literal #{(node.as(LiteralNode)).value}"
        (node.as(LiteralNode)).value
      when 5
        puts "#{child_vals} 5"
        if child_vals.size == 1
          puts "I give up"
          Int64.new(-1)
        else
          Int64.new(child_vals[0] > child_vals[1] ? 1 : 0)
        end
      when 6
        puts "#{child_vals} 6"
        Int64.new(child_vals[0] < child_vals[1] ? 1 : 0)
      when 7
        puts "#{child_vals} 7"
        Int64.new(child_vals[0] == child_vals[1] ? 1 : 0)
      else
        # Impossible
        puts "ERROR"
        Int64.new(-1)
      end
      puts a
      a
    end
    puts "Result #{my_val}"
  end

end


def read_packet(total_bits : String, start_index : Int64)
  puts "Reading #{start_index} of #{total_bits.size}"
  if start_index + 5 >= total_bits.size
     return { nil, Int64.new(total_bits.size) }
  end
  
  i = start_index
  version = Hex.binary_value(total_bits[i..(i+2)])
  i += 3
  type_id = Hex.binary_value(total_bits[i..(i+2)])
  i += 3

  puts "Node #{type_id}"
  case type_id
  # when 0
  #   i = start_index
  #   node = { EmptyNode.new(start_index), total_bits.size }
  #   puts node
  #   node
  when 4
    last_literal = false
    literal = ""
    while !last_literal
      last_literal = total_bits[i] == '0'
      i += 1
      literal += total_bits[i..(i+3)]
      i += 4
    end
    node = { LiteralNode.new(version, type_id, start_index, Hex.binary_value(literal)), i }
    # puts node
    node
    # Literal
  else
    # Operator
    length_type_id = total_bits[i]
    sub_nodes = [] of Node
    i += 1
    case length_type_id
    when '0'
      subpackets_length_in_bits = Hex.binary_value(total_bits[i...(i+15)])
      i += 15
      start_read = i
      puts "Starting #{start_read}"
      end_read = i + subpackets_length_in_bits
      puts "Ending #{end_read}"
      while i < end_read
        sub_node, end_index = read_packet(total_bits, i)
        i = end_index
        puts "Node #{type_id} : Index #{i} of #{end_read}"
        if !sub_node.nil?
          puts "Node #{type_id} adding #{sub_node.as(Node).type_id}"
          sub_nodes.push(sub_node.as(Node))
        else
          puts "Something is nil"
        end
      end
      i = end_read
    else
      subpacket_length_in_count = Hex.binary_value(total_bits[i...(i+11)])
      i += 11
      packets = Int64.new(0)
      puts "Endin packets #{subpacket_length_in_count}"
      while packets < subpacket_length_in_count
        puts "Packets #{packets}"
        sub_node, end_index = read_packet(total_bits, i)
        
        if !sub_node.nil?
          packets += 1
          # packets += sub_node.node_count
          puts "Packets Upped by #{sub_node.node_count} to #{packets} of #{subpacket_length_in_count}"
          i = end_index
          sub_nodes.push(sub_node.as(Node))
        else
          puts "Just setting to max."
          packets = subpacket_length_in_count
          i = end_index
        end
      end
    end
    puts "Completed Node #{type_id}"
    node = { OpNode.new(version, type_id, start_index, sub_nodes), i }
    # puts node
    node
  end
end

struct NodeInfo
  property version, type_id, start_index

  def initialize(@version : Int64, @type_id : Int64, @start_index : Int64)
  end
end

abstract class Node
  def initialize(version : Int64, type_id : Int64, start_index : Int64)
    @version = version
    @type_id = type_id
    @start_index = start_index
  end

  def version
    @version
  end

  def type_id
    @type_id
  end

  def start_index
    @start_index
  end

  def walk(&block : NodeInfo -> _)
    yield info
  end

  def info
    NodeInfo.new(@version, @type_id, @start_index)
  end

  def children
    [] of Node
  end

  def node_count : Int64
    Int64.new(1)
  end
end

class EmptyNode < Node
  def initialize(start_index : Int64)
    super(0, 0, start_index)
  end

  def walk(&block : NodeInfo -> _)
  end
end

class LiteralNode < Node
  @value : Int64
  def initialize(version : Int64, type_id : Int64, start_index : Int64, value : Int64)
    super(version, type_id, start_index)
    @value = value
  end

  def value
    @value
  end

  def walk(&block : NodeInfo -> _)
    puts "Literal yield"
    yield info
  end
end

class OpNode < Node
  @children : Array(Node)
  def initialize(version : Int64, type_id : Int64, start_index : Int64, children : Array(Node))
    super(version, type_id, start_index)
    @children = children
  end

  def children
    @children
  end

  def operator
    @operator
  end

  # def each(&block : Node -> _)
  #   (0...children.size).each do |i|
  #     children[i].each(&block)
  #   end
  #   # children.each {|c| c.each(&block) }
  #   yield self
  # end

  def walk(&block : NodeInfo -> _)
    puts "Op yield"
    yield info
    @children.each { |c| puts "Child #{c}" }
    # @children[0].walk(&block)
    # i = Int64.new(0)
    # Even this fails for the compiler. It cannot handle passing the block to children.
    # if false
    #   @children[0].walk(&block)
    # end
    
    # while i < 1
    #   children[i].walk(&block)
    #   i += 1
    # end
  end

  def node_count : Int64
    @children.reduce(Int64.new(1)) { |acc, v| acc + v.node_count }
  end
end

module Hex
  extend self

  # TODO This could use BitArray instead to compactly store this.
  def hex_hash()
    {'0' => "0000",
    '1' => "0001",
    '2' => "0010",
    '3' => "0011",
    '4' => "0100",
    '5' => "0101",
    '6' => "0110",
    '7' => "0111",
    '8' => "1000",
    '9' => "1001",
    'A' => "1010",
    'B' => "1011",
    'C' => "1100",
    'D' => "1101",
    'E' => "1110",
    'F' => "1111" }
  end

  def hex_value(char)
    hex_hash[char]
  end

  def binary_value(s)
    val = Int64.new(0)
    i = Int64.new(0)
    # puts s
    (0...(s.size)).each do |i|
      val += Int64.new(s[i]) * (Int64.new(2) ** (s.size - i - 1) )
    end
    # puts val
    val
  end
end

