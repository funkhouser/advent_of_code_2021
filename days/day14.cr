def day_14(part)
  puts "Day 14: https://adventofcode.com/2021/day/14"
  puts "Day 14 Input: https://adventofcode.com/2021/day/14/input"
  case part
  when "1"
    day_14_part_1()
  when "2"
    day_14_part_2()
  else
    day_14_part_1()
    day_14_part_2()
  end
end


def day_14_part_1()
  puts "Day 14: Part 1"
  input_file = "./inputs/day14"
  polymer = ""
  inserts = Hash(String,Char).new
  File.read_lines(input_file).each_with_index do |line, i|
    if i == 0
      polymer = line
    elsif !line.blank?
      inserts[line[0,2]] = line.chars[6]
    end
  end
  puts polymer
  puts inserts

  (0...10).each do |step_index|
    new_chars = ""
    polymer.chars.each_cons_pair do |a, b|
      new_chars += a
      if inserts.has_key?("#{a}#{b}")
        new_chars += inserts["#{a}#{b}"]
      end
    end
    new_chars += polymer.chars[-1]
    polymer = new_chars
    # puts polymer
  end

  counts = Hash(Char,Int32).new
  polymer.chars.each do |poly|
    counts[poly] = (counts[poly]? || 0) + 1
  end

  puts counts
  values = counts.values.sort
  puts "Max #{values[-1]} Min #{values[0]}"
  puts "Result #{values[-1] - values[0]}"
end

def day_14_part_2()
  puts "Day 14: Part 2"
  puts "Day 14: Part 1"
  input_file = "./inputs/day14"
  polymer = ""
  inserts = Hash(String,Char).new
  File.read_lines(input_file).each_with_index do |line, i|
    if i == 0
      polymer = line
    elsif !line.blank?
      inserts[line[0,2]] = line.chars[6]
    end
  end
  puts polymer
  puts inserts

  counts = Hash(String,Int64).new
  polymer.chars.each_cons_pair do |a, b|
    key = "#{a}#{b}"
    counts[key] = (counts[key]? || Int64.new(0)) + 1
  end
  puts counts
  # polymer.chars.each do |poly|
  #   counts[poly] = (counts[poly]? || 0) + 1
  # end
  (0...40).each do |step_index|
    puts step_index

    # Map to an array and then compact back to a hash
    raw_counts = counts.flat_map do |key, value|
      if inserts.has_key?(key)
        key0 = "#{key[0]}#{inserts[key]}"
        key1 = "#{inserts[key]}#{key[1]}"
        [
          {key0, value},
          {key1, value}
        ]
      else
        [{key, value}]
      end
    end

    new_counts = Hash(String,Int64).new
    raw_counts.each do |key, value|
      new_counts[key] = (new_counts[key]? || Int64.new(0)) + value
    end

    # puts raw_counts
    puts new_counts
    counts = new_counts
  end

  

  puts counts
  

  char_counts = Hash(Char,Int64).new
  counts.each do |key, value|
    char_counts[key[0]] = (char_counts[key[0]]? || Int64.new(0)) + value
    char_counts[key[1]] = (char_counts[key[1]]? || Int64.new(0)) + value
  end

  puts char_counts
  values = char_counts.values.sort

  min = (values[0] + 1) // 2
  max = (values[-1] + 1) // 2

  puts "Max #{max} Min #{min}"
  puts "Result #{max - min}"
end