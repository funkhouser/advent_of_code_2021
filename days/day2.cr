def day_2(part)
  puts "Day 2: https://adventofcode.com/2021/day/2"
  case part
  when "1"
    day_2_part_1()
  when "2"
    day_2_part_2()
  else
    day_2_part_1()
    day_2_part_2()
  end
end


def day_2_part_1()
  puts "Day 2: Part 1"
  puts "Determine depth and horizontal position from given sub commands"
  result = File.read_lines("./inputs/day2").each.reduce({0, 0}) do |position, command|
    direction = command.split(" ")[0]
    magnitude = Int32.new(command.split(" ")[1])
    case direction
    when "forward"
      {position[0] + magnitude, position[1]}  
    when "down"
      { position[0], position[1] + magnitude }
    when "up"
      { position[0], position[1] - magnitude }
    else
      { position[0], position[1] }
    end
  end

  puts "Location #{result}"
  puts "Answer: #{result[0] * result[1]}"
end

def day_2_part_2()
  puts "Day 2: Part 2"
  puts "Determine the position and the aim for the sub"

  result = File.read_lines("./inputs/day2").each.reduce({0, 0, 0}) do |position, command|
    direction = command.split(" ")[0]
    magnitude = Int32.new(command.split(" ")[1])
    case direction
    when "forward"
      { position[0] + magnitude, position[1] + position[2] * magnitude, position[2] }  
    when "down"
      { position[0], position[1], position[2] + magnitude }
    when "up"
      { position[0], position[1], position[2] - magnitude }
    else
      { position[0], position[1], position[2] }
    end
  end

  puts "Location #{result}"
  puts "Answer: #{result[0] * result[1]}"
end