def day_11(part)
  puts "Day 11: https://adventofcode.com/2021/day/11"
  puts "Day 11 Input: https://adventofcode.com/2021/day/11/input"
  case part
  when "1"
    day_11_part_1()
  when "2"
    day_11_part_2()
  else
    day_11_part_1()
    day_11_part_2()
  end
end


class Matrix(T) < Array(Array(T))
  def do_each(block)
    self.each.each(block)
  end

  def map(block)
    self.map {|row| row.map(block) }
  end
end

def day_11_part_1()
  puts "Day 11: Part 1"
  puts "Octopus energy levels!"
  input_file = "./inputs/day11"
  octo_grid : Array(Array(Int32)) = File.read_lines(input_file).map do |line|
    line.chars.map do |char|
      Int32.new(char)
    end
  end

  adjacency_matrix = [{-1,-1},{-1,0},{-1,1}, {0,-1},{0,1},{1,-1},{1,0},{1,1}]
  flashes = 0
  (0...100).each do |step_index|
    puts step_index + 1
    octo_grid = octo_grid.map { |row| row.map {|point| point + 1 } }
    stack_to_apply = Deque(Tuple(Int32, Int32)).new

    octo_grid.each_with_index do |row, row_index|
      row.each_with_index do |o, col_index|
        if o >= 10
          stack_to_apply.push({ row_index, col_index })
        end
      end
    end

    
    while !stack_to_apply.empty?
      point = stack_to_apply.pop()
      flashes += 1
      if point[0] < 0 || point[1] < 0
        puts "no"
      end
      octo_grid[point[0]][point[1]] = 0
      adjacency_matrix.each do |d|
        adj_val = matrix_val(octo_grid, {point[0] + d[0], point[1] + d[1]})
        if adj_val.nil? || adj_val == 0
          # Do nothing, already flashed or bad index
        elsif adj_val == 9
          stack_to_apply.push({point[0] + d[0], point[1] + d[1]})
          octo_grid[point[0] + d[0]][point[1] + d[1]] = adj_val + 1
        else
          octo_grid[point[0] + d[0]][point[1] + d[1]] = adj_val + 1
        end
      end
    end
    # octo_grid.each

    octo_grid.each {|r| puts r }
    puts ""
  end

  puts "Flashes #{flashes}"
end

def matrix_val(matrix, point)
  if matrix.nil? || point[0] < 0 || point[0] >= matrix.size || point[1] < 0 || point[1] >= matrix[0].size
    nil
  else
    matrix[point[0]][point[1]]
  end
end

def day_11_part_2()
  puts "Day 11: Part 2"
  puts "Day 11: Part 1"
  puts "Octopus energy levels!"
  input_file = "./inputs/day11"
  octo_grid : Array(Array(Int32)) = File.read_lines(input_file).map do |line|
    line.chars.map do |char|
      Int32.new(char)
    end
  end

  adjacency_matrix = [{-1,-1},{-1,0},{-1,1}, {0,-1},{0,1},{1,-1},{1,0},{1,1}]
  flashed_each_step = 0
  (0...10000).each do |step_index|
    puts step_index + 1
    flashed_each_step = 0
    octo_grid = octo_grid.map { |row| row.map {|point| point + 1 } }
    stack_to_apply = Deque(Tuple(Int32, Int32)).new

    octo_grid.each_with_index do |row, row_index|
      row.each_with_index do |o, col_index|
        if o >= 10
          stack_to_apply.push({ row_index, col_index })
        end
      end
    end

    
    while !stack_to_apply.empty?
      point = stack_to_apply.pop()
      flashed_each_step += 1
      if point[0] < 0 || point[1] < 0
        puts "no"
      end
      octo_grid[point[0]][point[1]] = 0
      adjacency_matrix.each do |d|
        adj_val = matrix_val(octo_grid, {point[0] + d[0], point[1] + d[1]})
        if adj_val.nil? || adj_val == 0
          # Do nothing, already flashed or bad index
        elsif adj_val == 9
          stack_to_apply.push({point[0] + d[0], point[1] + d[1]})
          octo_grid[point[0] + d[0]][point[1] + d[1]] = adj_val + 1
        else
          octo_grid[point[0] + d[0]][point[1] + d[1]] = adj_val + 1
        end
      end
    end
    if flashed_each_step == 100
      puts "All flashed!"
      puts "Result #{step_index + 1}"
      exit()
    end
    # octo_grid.each

    # octo_grid.each {|r| puts r }
    # puts ""
  end
end