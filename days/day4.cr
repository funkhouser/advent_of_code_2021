def day_4(part)
  puts "Day 4: https://adventofcode.com/2021/day/4"
  puts "Day 4 Input: https://adventofcode.com/2021/day/4/input"
  case part
  when "1"
    day_4_part_1()
  when "2"
    day_4_part_2()
  else
    day_4_part_1()
    day_4_part_2()
  end
end


def day_4_part_1()
  puts "Day 4: Part 1"
  puts "Given the bingo inputs and the bingo boards below, find the first winning board, sum the unmarked numbers, and multiply by last number called"

  file_lines = File.read_lines("./inputs/day4")
  header_line = file_lines[0]
  input_nums = header_line.split(",")

  board_lines = file_lines[1,file_lines.size]

  puts board_lines
  # Slice uses the enumerable implementation of map that relies on yielding elements
  # This means that everything is lazily computed. For example just printing the 
  # output will return the Iterable (printed) rather than computing the elements
  # Bouncing to an array with .to_a is a simple way to get around this for now.
  
  # Enumerable implementation:
  # def map(& : T -> U) : Array(U) forall U
  #   ary = [] of U
  #   each { |e| ary << yield e }
  #   ary
  # end

  sliced_boards = board_lines.each_slice(6).to_a
  boards = sliced_boards.map do |board_slice|
    board_slice[1,board_slice.size].to_a.map do |line|
      line.strip().split(/ +/)
    end
  end

  boards.each { |board| puts board }
  (1..(input_nums.size)).each do |input_index|
    current_numbers = input_nums[0,input_index].to_a
    puts current_numbers
    bingos = boards.map { |board| is_bingo?(board, current_numbers) }
    if bingos.any?
      puts "Bingo"
      puts bingos.to_a
      index = bingos.to_a.rindex { |x| x }
      if index.nil?
        exit
      end
      # One way to get around (Type|Nil) is to do suffix if exprs. Not great but interesting
      board = boards.to_a[index]
      unselected = board.flatten.reject { |num| current_numbers.rindex(num) }.to_a if board
      puts "Unselected #{unselected}"

      sum = unselected.reduce(0) { |acc, num| acc + Int32.new(num) } if unselected
      current_num = current_numbers[input_index - 1]
      if current_num.nil? || sum.nil?
        exit
      end
      puts "Unselected sum #{sum}"
      puts "Last called #{current_num}"
      puts "Result #{Int32.new(current_num) * sum}"
      exit
    end
  end
end

def is_bingo?(board, numbers)
  bool_board = board.map do |line|
    line.map { |board_num| numbers.rindex(board_num) }
  end
  col_or_row = (0...5).each.any? do |index|
    bool_board[index].all? || bool_board.map { |line| line[index] }.all?
  end

  # Sadly diagnols do not count
  #diag = ((0...5).each.all? { |i| bool_board[i][i] }) || ((0...5).each.all? { |i| bool_board[i][4 - i] })
  #diag || col_or_row
  col_or_row
end

def day_4_part_2()
  puts "Day 4: Part 2"
  puts "Same thing, but instead find the last board to win"
  file_lines = File.read_lines("./inputs/day4")
  header_line = file_lines[0]
  input_nums = header_line.split(",")

  board_lines = file_lines[1,file_lines.size]

  sliced_boards = board_lines.each_slice(6).to_a
  boards = sliced_boards.map do |board_slice|
    board_slice[1,board_slice.size].to_a.map do |line|
      line.strip().split(/ +/)
    end
  end

  boards.each { |board| puts board }
  previous_bingos = [] of Bool
  (1..(input_nums.size)).each do |input_index|
    previous_numbers = input_nums[0,input_index-1].to_a
    current_numbers = input_nums[0,input_index].to_a
    puts current_numbers
    bingos = boards.map { |board| is_bingo?(board, current_numbers) }
    if bingos.all?
      puts "Bingo"
      puts bingos.to_a
    
      index = previous_bingos.to_a.rindex { |x| !x }
      if index.nil?
        exit
      end
      # One way to get around (Type|Nil) is to do suffix if exprs. Not great but interesting
      board = boards.to_a[index]
      unselected = board.flatten.reject { |num| current_numbers.rindex(num) }.to_a if board
      puts "Unselected #{unselected}"

      sum = unselected.reduce(0) { |acc, num| acc + Int32.new(num) } if unselected
      current_num = current_numbers[input_index - 1]
      if current_num.nil? || sum.nil?
        exit
      end
      puts "Unselected sum #{sum}"
      puts "Last called #{current_num}"
      puts "Result #{Int32.new(current_num) * sum}"
      exit
    else
      previous_bingos = bingos
    end
  end
end