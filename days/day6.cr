def day_6(part)
  puts "Day 6: https://adventofcode.com/2021/day/6"
  puts "Day 6 Input: https://adventofcode.com/2021/day/6/input"
  case part
  when "1"
    day_6_part_1()
  when "2"
    day_6_part_2()
  else
    day_6_part_1()
    day_6_part_2()
  end
end

module MemoHolder
  extend self
  @@memos = {} of MemoIndex => Int64

  def memos
    @@memos
  end
end


def day_6_part_1()
  puts "Day 6: Part 1"
  puts "Exponential lantern fish oh my"
  puts "Should work well with some small memoizing"
  count_it(80)
end

def day_6_part_2()
  puts "Day 6: Part 2"
  count_it(256)
end

def latern_count(memo_index : MemoIndex, max_days : Int32) : Int64
  if MemoHolder.memos.has_key?(memo_index)
    MemoHolder.memos[memo_index]
  else 
    new_count = if memo_index.days == max_days
      Int64.new(1)
    elsif memo_index.timer == 0
      latern_count(MemoIndex.new(8, memo_index.days + 1), max_days) + 
      latern_count(MemoIndex.new(6, memo_index.days + 1), max_days)
    else
      latern_count(MemoIndex.new(memo_index.timer - 1, memo_index.days + 1), max_days)
    end
    MemoHolder.memos[memo_index] = new_count
    new_count
  end
end

struct MemoIndex
  property timer, days
  def initialize(@timer : Int32, @days : Int32)
  end
end

def count_it(days)
  MemoHolder.memos.clear
  input_file = "./inputs/day6"

  inputs = File.read(input_file).strip().split(",").map { |x| Int32.new(x) }

  # Fill the memoization
  (0..days).each do |i|
    (0..8).each do |timer|
      latern_count(MemoIndex.new(timer, days - i), days)
    end
  end

  result = inputs.reduce(Int64.new(0)) { |acc, timer| acc + latern_count(MemoIndex.new(timer, 0), days) }

  puts inputs
  puts result
end
