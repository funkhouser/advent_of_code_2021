def day_8(part)
  puts "Day 8: https://adventofcode.com/2021/day/8"
  puts "Day 8 Input: https://adventofcode.com/2021/day/8/input"
  case part
  when "1"
    day_8_part_1()
  when "2"
    day_8_part_2()
  else
    day_8_part_1()
    day_8_part_2()
  end
end


def day_8_part_1()
  puts "Day 8: Part 1"
  puts "Uh oh we got some wires crossed!"
  puts "The first part is just prep to parse the input and find the special chars in the output"

  input_file = "./inputs/day8"
  entries = File.read_lines(input_file).map do |line|
    LineEntry.new line.split(" ")[0..9], line.split(" ")[11..14]
  end

  special_display_sizes = Set{2, 3, 4, 7}
  special_entries = entries.reduce(0) do |acc, e| 
    acc + e.right.select { |display| special_display_sizes.includes?(display.size) }.size
  end
  puts "Result #{special_entries}"
end

struct LineEntry
  property left, right, solved_left, solved_right
  def initialize(@left : Array(String), @right : Array(String))
    @solved_left = Hash(Int32, Int32).new
    @solved_right = Hash(Int32, Int32).new
  end
end

module EntryModule
  extend self
  @@numbers = {
    0 => ['a', 'b', 'c', 'e', 'f', 'g'],
    1 => ['c', 'f' ],
    2 => ['a', 'c', 'd', 'e', 'g'],
    3 => ['a', 'c', 'd', 'f', 'g'],
    4 => ['b', 'c', 'd', 'f'],
    5 => ['a', 'b', 'd', 'f', 'g'],
    6 => ['a', 'b', 'd', 'e', 'f', 'g'],
    7 => ['a', 'c', 'f'],
    8 => ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
    9 => ['a', 'b', 'c', 'd', 'f', 'g']
  }

  def numbers
    @@numbers
  end
end



struct Entry
  property digits, solved, locations
  def initialize(@digits : Array(String))
    @solved = Hash(Int32, Int32).new
    @locations = Hash(Char, Char).new
  end
end

def day_8_part_2()
  puts "Day 8: Part 2"
  puts "Solve all of the segments and add up the 4 digit outputs"

  input_file = "./inputs/day8"
  entries = File.read_lines(input_file).map do |line|
    Entry.new line.split(" ")[0..9] + line.split(" ")[11..14]
  end

  # a = top
  # b = top-left
  # c = top-right
  # d = middle
  # e = bottom-left
  # f = bottom-right
  # g = bottom

  total = 0
  results = entries.map do |entry|
    # Naive: do up to n passes over the entry until we have the exact nums
    entry.digits.map_with_index do |digit, i|
      if i < 10
        case digit.size
        when 2
          solve_for(i, entry, 1, digit)
        when 3
          solve_for(i, entry, 7, digit)
        when 4
          solve_for(i, entry, 4, digit)
        when 7
          solve_for(i, entry, 8, digit)
        end
      end
      
    end

    #   case digit.size
    #   when 5
    #     #  2, 3, 5
    #     puts "2, 3, 5"
    #   when 6
    #     puts "0, 6, 9"
    #   end

    # puts entry

    a = (entry.digits[entry.solved[7]].chars - entry.digits[entry.solved[1]].chars)[0]
    right_chars = entry.digits.find("z") { |digit| digit.size == 2 }.chars
    six_chars = entry.digits.find("z") { |digit| digit.size == 6 && (right_chars & digit.chars).size == 1 }.chars
    f = (right_chars & six_chars)[0]
    c = (right_chars - six_chars)[0]

    db = entry.digits[entry.solved[4]].chars - entry.digits[entry.solved[7]].chars
    d = entry.digits.select { |digit| digit.size == 5 }.map { |digit| digit.chars }.reduce(db) { |acc, d| acc & d }[0]
    b = (db - [d])[0]
    g = ((entry.digits.select { |digit| digit.size == 5 }.map { |digit| digit.chars }.reduce { |acc, dc| acc & dc }) - [a, d])[0]
    e = (['a', 'b', 'c', 'd', 'e', 'f', 'g'] - [a, b, c, d, f, g])
    # puts e
    e = e[0]

    puts [a, b, c, d, e, f, g]
    values = {
      a => 'a',
      b => 'b',
      c => 'c',
      d => 'd',
      e => 'e',
      f => 'f',
      g => 'g'
    }

    result = 0
    entry.digits.each_with_index do |digit, i|
      if i >= 10
        multiplier = 10 ** (13 - i)
        actual_chars = digit.chars.map do |c|
          values[c]
        end
        my_num = (EntryModule.numbers.find({-1, ['z']}) do |key, value|
          value.size == actual_chars.size && value.sort == actual_chars.sort
          # (value - actual_chars).size == 0 && (value & actual_chars).size == value.size
        end)[0]
        result += multiplier * my_num
      end
    end

    total += result
    
  end

  puts "Result #{total}"
end

def solve_for(number : Int32, entry : Entry, i : Int32, digit : String)
  entry.solved[i] = number
  # The original idea of filtering out is not necessary since its a known pattern of all digits
  # digit.chars.each do |char|
  #   # TODO This should be the sub-set of possible locations
  #   entry.locations[char].select! { |possible| EntryModule.numbers[number].includes?(possible) }
  # end
end

# end


# This was not the way to do it, but in short was attempting to back-trackingly guess the characters based on what was valid.
# def attempt_solve(locations : Array(Tuple(Char, Array(Char)))) : (Hash(Char, Char) | Nil)
#   if locations.empty?
#     Hash(Char, Char).new
#   else
#     next_selection = locations[0]
#     puts next_selection
#     puts locations
#     found = next_selection[1].map do |possibility|
#       new_locations = locations.map { |char, possibilities| { char, possibilities - [possibility] } }
#       new_locations.reject! { |char, p| next_selection[0] == char }
#       # puts new_locations
#       if new_locations.nil?
#         nil
#       else 
#         attempt = attempt_solve(new_locations)
#         if attempt.nil?
#           nil
#         else
#           attempt[next_selection[0]] = possibility
#           attempt
#         end
#       end
#     end
#     found = found.compact
#     if found.size > 1
#       puts "What"
#       nil
#     elsif found.size == 1
#       found[0]
#     else
#       nil
#     end
#   end
# end