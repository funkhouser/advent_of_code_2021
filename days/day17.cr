def day_17(part)
  puts "Day 17: https://adventofcode.com/2021/day/17"
  puts "Day 17 Input: https://adventofcode.com/2021/day/17/input"
  case part
  when "1"
    day_17_part_1()
  when "2"
    day_17_part_2()
  else
    day_17_part_1()
    day_17_part_2()
  end
end


def day_17_part_1()
  puts "Day 17: Part 1"
  puts "Some fun with trajectories"
  puts "X-velocity changes by 1 towards 0, Y-velocity reduces by 1"

  puts "Shooting it with _style_, what is the highest possible result for firing?"
  puts "Range target area: x=117..164, y=-140..-89"

  example_range = [
    {20, 30},
    {-10, -5}
  ]

  target_range = [
    {117, 164},
    {-140, -89}
  ]

  range = target_range
  best_found = {0, 0}

  vels = (0..165).flat_map do |x|
    hits = (-200..300).select do |y|
      hits_target?(range, x, y)
    end

    hits.map { |y| {x, y} }
  end
  
  puts vels

  best_vel = vels.sort_by { |x, y| y }[-1]
  puts best_vel

  top_y = (0..best_vel[1]).reduce(0) { |acc, v| acc + v }
  top_x = best_vel[1] / 2
  
  puts [ top_x, top_y ]

  puts "Total vels #{vels.size}"
end


def hits_target?(range, x_i, y_i)
  x = 0
  y = 0
  x_vel = x_i
  y_vel = y_i
  step = 0
  while x <= range[0][1] && (y >= range[1][1] || y >= range[1][0])
    step += 1
    x += x_vel
    y += y_vel
    if in_range?(range, x, y)
      return true
    end
    x_vel = x_vel > 0 ? x_vel - 1 : x_vel
    y_vel -= 1
  end
  # puts "Out of range #{in_range?(range, x, y)} #{x_i} #{y_i} #{x} #{y}"
  
end

def in_range?(range, x, y)
  range[0][0] <= x && x <= range[0][1] && range[1][0] <= y && y <= range[1][1]
end

def becomes_vertical?(x_range, x_vel)
  vert_at = (x_vel * (x_vel + 1) / 2) 
  puts "#{x_vel} #{vert_at}"
  vert_at > x_range[0] && vert_at < x_range[1]
end

def day_17_part_2()
  puts "Day 17: Part 2"
  puts "Combined with Day 1"
end