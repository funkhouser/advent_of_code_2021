def day_7(part)
  puts "Day 7: https://adventofcode.com/2021/day/7"
  puts "Day 7 Input: https://adventofcode.com/2021/day/7/input"
  case part
  when "1"
    day_7_part_1()
  when "2"
    day_7_part_2()
  else
    day_7_part_1()
    day_7_part_2()
  end
end


def day_7_part_1()
  puts "Day 7: Part 1"
  puts "Crab sub, crab sub, making all the crab sub"
  puts "How much fuel is it to align on the least expensive position?"
  puts "Take the mean, round it, and sum the absolute diffs. Persumably a more difficult regression is part 2"
  puts "Because of integer programming this solution actually doesn't work. I'll need to research if there is a general solution to this"

  input_file = "./inputs/day7"
  inputs = File.read(input_file).strip().split(",").map { |x| Int32.new(x) }

  mean = inputs.reduce(0) { |acc, x| acc + x }.to_f32 / (inputs.size)
  sorted_inputs = inputs.sort
  # puts "Sorted Inputs #{sorted_inputs}"
  # puts "Mean #{mean}"
  # position = mean.round.to_i32
  # puts "Position #{position}"
  min_fuel = 2147483647
  min_position = -1
  ((inputs.min)..(inputs.max)).each do |position|
    fuel = inputs.reduce(0) { |acc, x| acc + (position - x).abs } 
    if fuel < min_fuel
      min_fuel = fuel
      min_position = position
    end
  end

  puts "Position #{min_position}"
  puts "Fuel #{min_fuel}"
end

def day_7_part_2()
  puts "Day 7: Part 2"
  puts "How much fuel is it to align on the least expensive position?"
  puts "Indeed, its a more complicated regression, specifically triangle numbers"

  input_file = "./inputs/day7"
  inputs = File.read(input_file).strip().split(",").map { |x| Int32.new(x) }

  mean = inputs.reduce(0) { |acc, x| acc + x }.to_f32 / (inputs.size)
  sorted_inputs = inputs.sort
  # puts "Sorted Inputs #{sorted_inputs}"
  # puts "Mean #{mean}"
  # position = mean.round.to_i32
  # puts "Position #{position}"
  min_fuel = 2147483647
  min_position = -1
  ((inputs.min)..(inputs.max)).each do |position|
    fuel = inputs.reduce(0) { |acc, x| acc + triangle((position - x).abs) } 
    if fuel < min_fuel
      min_fuel = fuel
      min_position = position
    end
  end

  puts "Position #{min_position}"
  puts "Fuel #{min_fuel}"
end

def triangle(num) : Int32
  ((num * (num + 1)) // 2)
end