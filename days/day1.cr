
def day_1(part)
  puts "Day 1: https://adventofcode.com/2021/day/1"
  case part
  when "1"
    day_1_part_1()
  when "2"
    day_1_part_2()
  else
    day_1_part_1()
    day_1_part_2()
  end
end


def day_1_part_1()
  puts "Day 1: Part 1"
  puts "How many depth measurements are larger than the previous?"

  ## Implicit close via closure
  content = File.open("./inputs/day1") do |file|
    file.gets_to_end
  end

  ## Shortcut to read entire
  content = File.read("./inputs/day1")
  
  ## Could just use a reduce in-line but its fun to run an accumulate
  result_acc = File.read_lines("./inputs/day1").each.accumulate({0, "1000000"}) do |count_tuple, measurement| 
    if Int32.new(measurement) > Int32.new(count_tuple[1])
      { count_tuple[0] + 1, measurement }
    else
      { count_tuple[0], measurement}
    end
  end

  ## That gets reduced down. Notice that the acc parameter is unused here.
  puts result_acc.reduce { |acc, i| i }
end

def day_1_part_2()
  puts "Day 1: Part 2"

  result = File.read_lines("./inputs/day1").each.reduce({0, "1000000", "1000000", "1000000"}) do |count_tuple, measurement| 
    if Int32.new(measurement) > Int32.new(count_tuple[1])
      { count_tuple[0] + 1, count_tuple[2], count_tuple[3], measurement }
    else
      { count_tuple[0], count_tuple[2], count_tuple[3], measurement }
    end
  end

  puts result
end