def day_3(part)
  puts "Day 3: https://adventofcode.com/2021/day/3"
  puts "Day 3 Input: https://adventofcode.com/2021/day/3/input"
  case part
  when "1"
    day_3_part_1()
  when "2"
    day_3_part_2()
  else
    day_3_part_1()
    day_3_part_2()
  end
end


def day_3_part_1()
  puts "Day 3: Part 1"
  puts "Submarine diagnostics: Finding gamma and epsilon rates"
  puts "Gamma = most common bit in each column"
  puts "Epsilon = least common bit in each column"

  # results = Hash(Int32, Int32).new
  results = {} of Int32 => Int32
  results[-1] = 0
  result = File.read_lines("./inputs/day3").each do |line|
    results[-1] += 1
    #  Put allows for a block to be called if the value didn't exist
    # results.put(-1, results.fetch(-1, 0) + 1) {}
    line.chars.map_with_index() do |char, index|
      case char
      when '1'
        results[index] = results.fetch(index, 0) + 1
      end
    end
  end

  size = results[-1]
  num_size = results.size - 2
  gamma = 0
  epsilon = 0
  results.each do |key_value|
    if key_value[0] == -1
      # Do nothing
    elsif (key_value[1] * 2) > size
      gamma += 2 ** (num_size - key_value[0])
    else 
      epsilon += 2 ** (num_size - key_value[0])
    end
  end

  puts results
  puts "Gamma #{gamma}"
  puts "Epsilon #{epsilon}"
  puts "Result: #{gamma * epsilon}"
end

def day_3_part_2()
  puts "Day 3: Part 2"
  puts "Right, the clever way to go about this is clearly to sort them first"

  input_name = "./inputs/day3"
  oxy_lines = File.read_lines(input_name).sort
  line_size = oxy_lines[0].chars.size
  oxy_prefix = ""
  (0...line_size).each do |index|
    puts oxy_prefix
    oxy_lines = oxy_lines.select { |line| line.starts_with?(oxy_prefix) }
    if oxy_lines.size > 1
      mid = oxy_lines[Int32.new(oxy_lines.size / 2)]
      oxy_prefix = oxy_prefix + mid.chars[index]
    else
      oxy_prefix = oxy_lines[0]
    end
  end

  puts "Oxy #{oxy_prefix}"

  co_lines = File.read_lines(input_name).sort

  co_prefix = ""
  (0...line_size).each do |index|
    puts co_prefix
    co_lines = co_lines.select { |line| line.starts_with?(co_prefix) }
    if co_lines.size > 1
      mid = co_lines[Int32.new(co_lines.size / 2)]
      co_prefix = co_prefix + (mid.chars[index] == '1' ? '0' : '1')
    else
      co_prefix = co_lines[0]
    end
  end

  puts "CO #{co_prefix}"

  puts "Oxy Decimal #{binary_to_num(oxy_prefix)}"
  puts "CO Decimal #{binary_to_num(co_prefix)}"
  puts "Result #{binary_to_num(oxy_prefix) * binary_to_num(co_prefix)}"
end


def binary_to_num(binary_string)
  size = binary_string.size
  binary_string.chars.each_with_index.reduce(0) do |acc, (char, index) |
    binary_string[index] == '1' ? acc + (2 ** (size - index - 1)) : acc
  end
end