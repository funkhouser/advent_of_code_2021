def day_10(part)
  puts "Day 10: https://adventofcode.com/2021/day/10"
  puts "Day 10 Input: https://adventofcode.com/2021/day/10/input"
  case part
  when "1"
    day_10_part_1()
  when "2"
    day_10_part_2()
  else
    day_10_part_1()
    day_10_part_2()
  end
end

def day_10_part_1
  puts "Day 10: Part 1"

  matches = {
    '{' => '}',
    '(' => ')',
    '[' => ']',
    '<' => '>',
  }

  scores = {
    '}' => 1197,
    ')' => 3,
    '>' => 25137,
    ']' => 57,
    nil => 0
  }
  # open = ['{', '[', '(', '<']
  # close = ['}', ']', '(', '>']
  input_file = "./inputs/day10"
  lines = File.read_lines(input_file).map do |line|
    stack = Deque(Char).new
    status = "Incomplete"
    corrupt_char : (Char | Nil) = nil
    line.chars.each do |char|
      # puts "#{char} #{status} #{stack}"
      if matches.keys.includes?(char)
        stack.push(char)
      else
        last_char = stack.pop()
        if !(matches[last_char] == char)
          puts "Corrupted"
          status = "Corrupted"
          corrupt_char = char
        end
      end
    end
    { line, status, corrupt_char }
  end

  result = lines.reduce(0) { |acc, line| acc + scores[line[2]] }
  puts "Result #{result}"
end

def day_10_part_2
  puts "Day 10: Part 2"

  matches = {
    '{' => '}',
    '(' => ')',
    '[' => ']',
    '<' => '>',
  }


  # open = ['{', '[', '(', '<']
  # close = ['}', ']', '(', '>']
  input_file = "./inputs/day10"
  lines = File.read_lines(input_file).map do |line|
    stack = Deque(Char).new
    status = "Incomplete"
    corrupt_char : (Char | Nil) = nil
    line.chars.each do |char|
      # puts "#{char} #{status} #{stack}"
      if matches.keys.includes?(char)
        stack.push(char)
      else
        last_char = stack.pop()
        if !(matches[last_char] == char)
          # puts "Corrupted"
          status = "Corrupted"
          corrupt_char = char
        end
      end
    end
    { line, status, stack }
  end

  a : Int64 = 0
  result = lines.select { |line| line[1] == "Incomplete" }.map { |line| do_score(line[2]) }.sort
  median = result[(result.size // 2)]

  # result = lines.reduce(0) { |acc, line| acc + scores[line[2]] }
  puts "Result #{result}"
  puts "Result size #{result.size}"
  puts "Middle #{(result.size // 2)}"
  puts "Median #{median}"
end

def do_score(stack : Deque(Char)) : Int64
  scores = {
    '{' => 3,
    '(' => 1,
    '<' => 4,
    '[' => 2
  }
  
  score : Int64 = 0
  while !stack.empty?
    char = stack.pop()
    score = score * 5 + scores[char]
  end
  score
end
