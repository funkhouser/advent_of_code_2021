def day_12(part)
  puts "Day 12: https://adventofcode.com/2021/day/12"
  puts "Day 12 Input: https://adventofcode.com/2021/day/12/input"
  case part
  when "1"
    day_12_part_1()
  when "2"
    day_12_part_2()
  else
    day_12_part_1()
    day_12_part_2()
  end
end


def day_12_part_1()
  puts "Day 12: Part 1"
  puts "Path finding in the caves"

  input_file = "./inputs/day12"
  connections = File.read_lines(input_file).map do |line|
    { Cave.new(line.strip().split("-")[0]), Cave.new(line.strip().split("-")[1]) }
  end

  node = Cave.new("start")

  total_paths = find_paths([node], node, connections)
  total_paths.each { |path| puts path }
  puts "Result #{total_paths.size}"
end

def find_paths(current_path : Array(Cave), current : Cave, connections : Array(Tuple(Cave,Cave))) : Array(Array(Cave))
  if current.key == "end"
    [current_path]
  else
    valid_connects = connections.select do |c|
      connects = c[0] == current || c[1] == current
      if connects
        other = if c[0] == current
          c[1]
        else
          c[0]
        end
  
        dupe_path = current_path.each_cons(2).any? do |path|
          path[0] == current && path[1] == other
        end
  
        dupe_small_node = current_path.includes?(other) && !other.large?
  
        !dupe_path && !dupe_small_node
      else
        false
      end
    end
    
    valid_connects.flat_map do |c|
      if c[0] == current
        find_paths(current_path + [c[1]], c[1], connections)
      else
        find_paths(current_path + [c[0]], c[0], connections)
      end
    end
  end 
end

struct Cave
  property key

  def initialize(key : String)
    @key = key
  end

  def large?
    @key.upcase == @key
  end

  def inspect(w)
    w.puts @key
  end

  def <=>(other)
    key <=> other.key
  end
end

def day_12_part_2()
  puts "Day 12: Part 2"
  input_file = "./inputs/day12"
  connections = File.read_lines(input_file).map do |line|
    { Cave.new(line.strip().split("-")[0]), Cave.new(line.strip().split("-")[1]) }
  end

  node = Cave.new("start")

  total_paths = find_paths_2([node], node, connections)
  total_paths.sort.each { |path| puts path }
  puts "Result #{total_paths.size}"
end


def find_paths_2(current_path : Array(Cave), current : Cave, connections : Array(Tuple(Cave,Cave))) : Array(Array(Cave))
  if current.key == "end"
    [current_path]
  else
    valid_connects = connections.select do |c|
      connects = c[0] == current || c[1] == current
      if connects
        other = if c[0] == current
          c[1]
        else
          c[0]
        end
  
        small_caves = current_path.select { |p| !p.large? } 
        previous_dupe_small_node = small_caves.size != small_caves.uniq.size

        # dupe_path = current_path.each_cons(2).any? do |path|
        #   path[0] == current && path[1] == other
        # end  
        dupe_path = false
        # dupe_small_node = current_path.includes?(other) && !other.large?
        dupe_small_node = if previous_dupe_small_node
          !(!current_path.includes?(other) || other.large?)
        else
          !(current_path.select {|p| p == other }.size < 2 || other.large?)
        end

        !dupe_path && !dupe_small_node && !(other.key == "start")
      else
        false
      end
    end
    
    valid_connects.flat_map do |c|
      if c[0] == current
        find_paths_2(current_path + [c[1]], c[1], connections)
      else
        find_paths_2(current_path + [c[0]], c[0], connections)
      end
    end
  end 
end


def find_paths_all(current_path : Array(Cave), current : Cave, connections : Array(Tuple(Cave,Cave))) : Array(Array(Cave))
  if current.key == "end"
    [current_path]
  else
    valid_connects = connections.select do |c|
      (c[0] == current || c[1] == current) && !(
        # For now, specify that connections cannot be traversed twice
        # in the same direction
        # This makes sense, given that no connections are between large nodes
        # and we don't want cycles
        current_path.each_cons(2).any? do |path|
          a = path[0]
          b = path[1]
          if current == c[0]
            a == c[0] && b == c[1]
          else
            a == c[1] && b == c[0]
          end
        end)
    end
    
    valid_connects.flat_map do |c|
      if c[0] == current
        find_paths_all(current_path + [c[1]], c[1], connections)
      else
        find_paths_all(current_path + [c[0]], c[0], connections)
      end
    end
  end 
end