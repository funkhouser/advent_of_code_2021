def day_9(part)
  puts "Day 9: https://adventofcode.com/2021/day/9"
  puts "Day 9 Input: https://adventofcode.com/2021/day/9/input"
  case part
  when "1"
    day_9_part_1()
  when "2"
    day_9_part_2()
  else
    day_9_part_1()
    day_9_part_2()
  end
end


def day_9_part_1()
  puts "Day 9: Part 1"
  input_file = "./inputs/day9"
  lines = File.read_lines(input_file).map { |line| line.chars.map { |c| Int32.new(c) } }
  risk_levels = 0
  deltas = [[-1, 0], [1, 0], [0, -1], [0, 1]]
  lines.each_with_index do |line, i|
    line.each_with_index do |level, j|
      if deltas.all? { |d| level < (value_or_nil(value_or_nil(lines, i + d[0]), j + d[1]) || 10) }
        risk_levels += level + 1
      end
    end
  end
  puts "Result #{risk_levels}"
end


def value_or_nil(array, index)
  if array.nil? || index < 0 || index >= array.size
    nil
  else
    array[index]
  end
end

def matrix_val(matrix, point)
  if matrix.nil? || point[0] < 0 || point[0] >= matrix.size || point[1] < 0 || point[1] >= matrix[0].size
    nil
  else
    matrix[point[0]][point[1]]
  end
end

def day_9_part_2()
  puts "Day 9: Part 2"

  input_file = "./inputs/day9"
  lines = File.read_lines(input_file).map { |line| line.chars.map { |c| Int32.new(c) } }
  
  basins = [] of Set(Tuple(Int32, Int32))
  # Basin search: start from all of the existing basins and traverse outward to all points each step
  #   If already in basin, lower, or 9, do nothing
  #   else add it to the basin and traverse
  deltas = [{-1, 0}, {1, 0}, {0, -1}, {0, 1}]
  lines.each_with_index do |line, i|
    line.each_with_index do |level, j|
      if deltas.all? { |d| level < (value_or_nil(value_or_nil(lines, i + d[0]), j + d[1]) || 10) }
        basins += [[{i, j}].to_set]
      end
    end
  end

  puts "Basins #{basins}"

  expanded_basins = (basins).map do |basin|
    # basin = basins[basin_index].not_nil!
    expand_from(lines, basin.first)
  end

  puts expanded_basins

  top_basins = expanded_basins.sort_by {|b| b.size }[-3..-1]
  puts "Top basins #{top_basins}"
  puts "Top basin sizes #{top_basins.map { |b| b.size}}"
  puts "Result #{top_basins.reduce(1) { |acc, b| acc * b.size}}"

end

def expand_from(lines, point : Tuple(Int32, Int32)) : Set(Tuple(Int32, Int32))
  deltas = [{-1, 0}, {1, 0}, {0, -1}, {0, 1}]
  basin = Set(Tuple(Int32, Int32)).new
  # search_queue = Deque.new(deltas.map { |d| {point[0] + d[0], point[1] + d[1]}})
  search_queue = Deque(Tuple(Int32, Int32)).new()
  # basin.add(point)
  search_queue.push(point)
  while !(search_queue.empty?)
    # puts search_queue
    search_point = search_queue.pop
    search_point_val = matrix_val(lines, search_point)
    # puts "#{search_point} #{search_point_val}"
    if !(search_point_val.nil?)
      basin.add(search_point)
      deltas.each do |d|
        delta_point = {search_point[0] + d[0], search_point[1] + d[1]}
        if basin.includes?(delta_point) || search_queue.includes?(delta_point)
          # Do nothing
        else
          delta_point_val = matrix_val(lines, delta_point) || 9
          if delta_point_val != 9 && search_point_val < delta_point_val
            search_queue.push(delta_point)
            # basin.add(delta_point)
          end
        end
      end
    end
  end

  basin
  # if basin.includes?(point) || point_val == 9
  #   basin
  # else
  #   expanded : Set(Tuple(Int32, Int32)) = (deltas.flat_map do |d|
  #     delta_point = {point[0] + d[0], point[1] + d[1]}
  #     delta_point_val = matrix_val(lines, delta_point) || 9
      
  #     if !basin.includes?(delta_point) && delta_point_val > point_val
  #       expand_from(lines, basin + [point].to_set, delta_point)
  #     else
  #       nil
  #     end
  #   end).compact.to_set.reduce {|acc, b| acc + b }
  #   expanded + basin
  # end
  # deltas.map do |d|
  #   delta_point = basin + point
  #   delta_point_val = (value_or_nil(value_or_nil(lines, i + d[0]), j + d[1])) || 9
  #   if basin.includes?(delta_point) || delta_point_val == 9
  #     # Do nothing
  #   else
  #     expand_from(lines, basin + [point], delta_point)
  #   end
  # end
end