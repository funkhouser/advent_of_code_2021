









require "option_parser"
require "./days/*"

day_to_run = "0"
part_to_run = ""

OptionParser.parse do |parser|
  parser.banner = "Advent of Code 2021"

  parser.on "-d DAY", "--day=DAY", "Select which day you want to run" do |day|
    day_to_run = day
  end

  parser.on "-p PART", "--part=PART", "Select which part to run (1 or 2), defaults to both" do |part|
    part_to_run = part
  end
end


case day_to_run
when "1"
  day_1(part_to_run)
when "2"
  day_2(part_to_run)
when "3"
  day_3(part_to_run)
when "4"
  day_4(part_to_run)
when "5"
  day_5(part_to_run)
when "6"
  day_6(part_to_run)
when "7"
  day_7(part_to_run)
when "8"
  day_8(part_to_run)
when "9"
  day_9(part_to_run)
when "10"
  day_10(part_to_run)
when "11"
  day_11(part_to_run)
when "12"
  day_12(part_to_run)
when "13"
  day_13(part_to_run)
when "14"
  day_14(part_to_run)
when "15"
  day_15(part_to_run)
when "16"
  day_16(part_to_run)
when "17"
  day_17(part_to_run)
## Insert new day line
else
  puts "That day is not available yet"
end